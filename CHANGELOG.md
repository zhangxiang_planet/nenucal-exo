# Change Log

## 0.2.1 - 2022-10-19

- [Fixed] In get_ms_freqs(), use reshape instead of squeeze()

## 0.2 - 2022-07-24

- [New] Implement N2 data level with MS split in time slices over the different nodes
- [Fixed] Improve solution smoothing.
- [Fixed] Fix flagtool loading wrong msutils module.
- [Fixed] Fix modeltool loading wrong skymodel module.
- [Fixed] soltool: improve opening solution file without directions.
- [Fixed] Fix frequency range flagging.

## 0.2 (RC1) - 2022-01-13

- [Fixed] skymodel.edit_model: If min_elevation_patch <= 0, do not check patch for elevation
- [Fixed] fix aoquality command line

## 0.2 (beta) - 2021-11-30

- [New] add imgpipe command line tool.
- [New] add the option skymodel.int_ateam_sky_model to set a different Ateam sky model than the default one.
- [New] add the possibility to subtract without applying a calibration table by specifying an empty cal.parmdb.
- [New] add predict task.
- [New] add the option skymodel.app_sky_model_file to set a specific main field apparent sky model.
- [Changed] improve "modeltool build" messages.
- [Changed] update Ateam sky model

## 0.1.11  - 2021-07-13

- [Changed] update dependencies to allow python >=3.6, astropy >=3.2, keyring>=20, and include losoto from pypi. LSMtool still not include you have to install it with pip afterwards.

## 0.1.10  - 2021-07-01

- [Fixed] SkyModel.get_directions() was not always returning the correct patch

## 0.1.9  - 2021-05-18

- [Changed] soltool smooth: use astropy convolve function to smooth the data, it should be more reliable for missing data.

## 0.1.8  - 2021-04-30

- [Fixed] update dependencies

## 0.1.7  - 2021-04-30

- [Added] nenucal.msutils has been moved from libpipe.msutils.
